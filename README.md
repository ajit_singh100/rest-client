<<<<<<< README.md
# Restful
## A Rest Client for Go (Golang)
========
## An extremely simple to use, lightweight, yet powerful REST Client
 
## Motivation
TNeed to add more files in the repositoryhe G http standard library is a great library, but it might sometimes be a bit too low level to use,
and it doesn't offer features like fork-join requests for better performance, response caching based on headers,
and the possibility to mockup responses.

## Features and Roadmap
### v0.1
* `GET`, `POST`, `PUT`, `PATCH`, `DELETE`, `HEAD` & `OPTIONS` HTTP verbs
* Dead simple, synchronous requests
* Automatic caching of hosts connections
* **Response Caching**, based on response headers (cache-control, last-modified, etag, expires)
* Local caching strategies: TTL, LRU & Max Byte Size.
* Mockups!
* Fork-Join request pattern, for sending many requests concurrently, improving client perfomance.
* Async request pattern.
* Request Body can be `string`, `[]byte`, `struct` & `map`.
* Automatic marshal and unmarshal for `JSON` and `XML` Content-Type. Default JSON.
* Full access to http.Response object.
* Retries
* BasicAuth
* UserAgent
* Gzip support
* HTTP/2 support (automatic with Go +1.6)

### v0.2
* Connection usage metrics
* Response Time metrics
* Brotli Content-Encoding Responses support - http://google-opensource.blogspot.com.ar/2015/09/introducing-brotli-new-compression.html

### v0.3
* Custom Root Certificates and Client Certificates
* Testing +95%

### v0.4
* Plugable external caches like Memcached

## Caching
Caching is done by two strategies working together: Time To Live (TTL) and
Least Recently Used (LRU). Objects are inserted in the cache based on
Response Headers. You can establish a maximum Memory Size for the cache
and objects are flushed based on time expiration (TTL) or by hitting the maximum
memory limit. In the last case, least accessed objects will be removed first.

## Examples

### Installation
clone en tu gopath o goroot
```shell
git clone https://github.com/mercadolibre/golang-restclient/
```

### Importing
```go
import "github.com/mercadolibre/golang-restclient/rest"
```

### Simple GET

```go
resp := rest.Get("https://api.restfulsite.com/resource")
```

### Simple POST

```go
// Using a `string` as body
resp := rest.Post("https://api.restfulsite.com/resource", "Body")
```

### Simple POST, with Struct Body

```go
type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

body := new(User)
body.Id = 1
body.Name = "Hernan"

// body will be marshall as JSON
resp := rest.Post("https://api.restfulsite.com/resource/1", body)
fmt.Println(resp)
```

### Fork Join
ForkJoin let you **fork** requests, and **wait** until all of them have return.

Concurrent has methods for Get, Post, Put, Patch, Delete, Head & Options,
with the almost the same API as the synchronous methods.
The difference is that these methods return a FutureResponse, which holds a pointer to
Response. Response inside FutureResponse is nil until request has finished.

```go
var f [3]*rest.FutureResponse

// ForkJoin will send all requests concurrently
// and will wait until all requests have their correspondent responses
rest.ForkJoin(func(c *rest.Concurrent) {
	f[0] = c.Get("https://api.restfulsite.com/resource/1")
	f[1] = c.Get("https://api.restfulsite.com/resource/2")
	f[2] = c.Get("https://api.restfulsite.com/resource/3")
})

for i := range f {
  if f[i].Response().StatusCode == http.StatusOK {
    fmt.Println(f[i].Response())
  }
}
```

### Async
Async let you make Restful requests in an **asynchronous** way, without blocking
the go routine calling the Async function.

Whenever the Response is ready, the **f** function will be called back.
```go
// This won't be blocked.
rest.AsyncGet("https://api.restfulsite.com/user", func(r *rest.Response) {
	if r.StatusCode == http.StatusOK {
		fmt.Println(r)
	}
})

// This will be printed first.
fmt.Println("print first")
```

### Defaults
* Headers: keep-alive, Cache-Control: no-cache
* Timeout: 2 seconds
* ContentType: JSON (for body requests in POST, PUT and PATCH)
* Cache: enable
* Cache Size: 1GB
* Idle Connections Per Host: 2 (the default of http.net package)
* HTTP/2: automatic with Go 1.6
* Gzip: automatic support for gzip responses

### RequestBuilder
RequestBuilder gives you the power to go beyond defaults.
It is possible to set up headers, timeout, baseURL, proxy, contentType, not to use
cache, directly disabling timeout (in an explicit way), and setting max idle connections.
```go

// You can reuse in every RequestBuilder
customPool := &rest.CustomPool{
	MaxIdleConnsPerHost: 100,
}

headers := make(http.Header)
headers.Add("myHeader", "myValue")

var rb = rest.RequestBuilder{
	Headers:             headers,
	Timeout:             200 * time.Millisecond,
	BaseURL:             "https://baseURL",
	Proxy:               "http://myproxy",
	ContentType:         rest.JSON,
	DisableCache:        false,
	DisableTimeout:      false,
	MaxIdleConnsPerHost: 10,
	CustomPool:     customPool,
}

resp := rb.Get("/mypath")
```

### Mockups
When using mockups all requests will be sent to the mockup server.
To activate the mockup *environment* you have two ways: using the flag -mock
```
go test -mock
```

Or by programmatically starting the mockup server
```
StartMockupServer()
```
#### A mockup example
```go
myURL := "http://mytest.com/foo"

myHeaders := make(http.Header)
myHeaders.Add("Hello", "world")

mock := rest.Mock{
	URL:          myURL,
	HTTPMethod:   http.MethodGet,
	ReqHeaders:   myHeaders,
	RespHTTPCode: http.StatusOK,
	RespBody:     "foo",
}

rest.AddMockups(&mock)

v := rest.Get(myURL)

```
=======
# Rest-Client



## Getting started

Add few more line to test Add more data in the file

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ajit_singh100/rest-client.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ajit_singh100/rest-client/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> README.md
